package main

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"strconv"

	"github.com/go-redis/redis/v8"
	"github.com/gorilla/mux"
)

var client *redis.Client

type Task struct {
	ID   int    `json:"id"`
	Text string `json:"text"`
}

const redisCreds = "redis:6379"

func main() {
	client = redis.NewClient(&redis.Options{
		Addr: redisCreds,
	})

	fmt.Printf("redis credential %s\n", redisCreds)

	defer client.Close()

	r := mux.NewRouter()

	r.HandleFunc("/tasks", getTasks).Methods("GET")
	r.HandleFunc("/tasks/{id}", getTask).Methods("GET")
	r.HandleFunc("/tasks", createTask).Methods("POST")
	r.HandleFunc("/tasks/{id}", deleteTask).Methods("DELETE")

	r.HandleFunc("/version", getVersion).Methods("GET")

	http.Handle("/", r)

	port := ":8080"
	fmt.Printf("Todo List server up and running on port %s...\n", port)
	http.ListenAndServe(port, nil)
}

func getTasks(response http.ResponseWriter, request *http.Request) {
	ctx := context.Background()
	keys, err := client.Keys(ctx, "task:*").Result()
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(fmt.Sprintf("Error getting tasks: %v", err)))
		return
	}

	var tasks []Task
	for _, key := range keys {
		taskID, _ := strconv.Atoi(key[5:])
		taskValue, err := client.Get(ctx, key).Result()
		if err != nil {
			response.WriteHeader(http.StatusInternalServerError)
			response.Write([]byte(fmt.Sprintf("Error getting task: %v", err)))
			return
		}

		tasks = append(tasks, Task{ID: taskID, Text: taskValue})
	}

	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(http.StatusOK)
	json.NewEncoder(response).Encode(tasks)
}

func getTask(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	taskID, err := strconv.Atoi(vars["id"])
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte("Invalid task ID"))
		return
	}

	ctx := context.Background()
	taskValue, err := client.Get(ctx, fmt.Sprintf("task:%d", taskID)).Result()
	if err == redis.Nil {
		response.WriteHeader(http.StatusNotFound)
		response.Write([]byte("Task not found"))
		return
	} else if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(fmt.Sprintf("Error getting task: %v", err)))
		return
	}

	task := Task{ID: taskID, Text: taskValue}
	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(http.StatusOK)
	json.NewEncoder(response).Encode(task)
}

func createTask(response http.ResponseWriter, request *http.Request) {
	var task Task
	err := json.NewDecoder(request.Body).Decode(&task)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte("Invalid task data"))
		return
	}

	ctx := context.Background()
	err = client.Set(ctx, fmt.Sprintf("task:%d", task.ID), task.Text, 0).Err()
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(fmt.Sprintf("Error creating task: %v", err)))
		return
	}

	response.WriteHeader(http.StatusCreated)
	// response.Write([]byte("Task created"))
	response.Write([]byte(task.Text))

}

func deleteTask(response http.ResponseWriter, request *http.Request) {
	vars := mux.Vars(request)
	taskID, err := strconv.Atoi(vars["id"])
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		response.Write([]byte("Invalid task ID"))
		return
	}

	ctx := context.Background()
	deleted, err := client.Del(ctx, fmt.Sprintf("task:%d", taskID)).Result()
	if err != nil {
		response.WriteHeader(http.StatusInternalServerError)
		response.Write([]byte(fmt.Sprintf("Error deleting task: %v", err)))
		return
	}

	if deleted == 0 {
		response.WriteHeader(http.StatusNotFound)
		response.Write([]byte("Task not found"))
		return
	}

	response.WriteHeader(http.StatusOK)
	response.Write([]byte("Task deleted"))
}

func getVersion(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	response.WriteHeader(http.StatusOK)
	response.Write([]byte("develop version"))
	return
}
